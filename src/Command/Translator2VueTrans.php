<?php
/**
 * Created by PhpStorm.
 * User: flazzarotto
 * Date: 10/04/18
 * Time: 12:23
 */

namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Translation\Loader\XliffFileLoader;


class Translator2VueTrans extends Command
{

    private $xliffFileLoader;

    public function __construct()
    {
        parent::__construct();
        $this->xliffFileLoader = new XliffFileLoader();
    }

    protected function configure()
    {
        $this->setName('vue:trans:dump');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dir = opendir($path = __DIR__ . '/../../translations');

        $trans = [];

        $number = 0;

        $writer = $this->output($output);

        $writer('Dumping JS translations...');



        while (($file = readdir($dir)) !== false) {
            if (is_dir($file)) {
                continue;
            }
            $mime = mime_content_type($path . '/' . $file);

            if ($mime === 'text/xml') {

                preg_match('#^(\w+)\.(\w+)\.(\w+)$#', $file, $matches);
                list(, $domain, $locale) = $matches;

                $loader = $this->xliffFileLoader->load($path . '/' . $file, $locale, $domain);

                foreach ($loader->all() as $data) {

                    foreach ($data as $source => $target) {

                        $number++;
                        $id = $loader->getMetadata($source, $domain)['id'];
                        $trans[$locale][$id] =
                            trim(
                                // replace explicit interval pluralization
                                preg_replace(
                                    '#((^)|(\|))\s*((?:{[0-9]+(\s*,\s*[0-9])*})|[[\]]-?((?:[0-9]+)|(?:Inf)),-?((?:[0-9]+)|(?:Inf))[[\]])\s*#',
                                    '$1',
                                    // replace variables delimiters
                                    preg_replace('#%([a-zA-Z_][_a-zA-Z0-9]*)%#', '{$1}',
                                        // replace all (single / multiple) space characters with space
                                        preg_replace('#\s+#',' ',$target)
                                    )
                                )
                            );

                        if ($output->getVerbosity() > OutputInterface::VERBOSITY_NORMAL) {
                            $writer('Generating translation for <info>'.$id.'</info>');
                        }
                    }
                }

            }

        }

        $outputTranslationFile = __DIR__ . '/../../assets/js/data/translations.json';
        file_put_contents($outputTranslationFile, json_encode($trans, JSON_UNESCAPED_UNICODE));

        $writer('Translation dump done: '.$number.' translations generated.');
    }

    /**
     * @param OutputInterface $output
     * @return \Closure
     */
    private function output(OutputInterface $output) {
        return function(string $message) use ($output) {
            if (!$output->isQuiet()) {
                $output->writeln('<info>vue:trans:dump</info> '.$message);
            }
        };
    }

}
