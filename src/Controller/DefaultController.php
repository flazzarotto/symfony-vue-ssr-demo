<?php

namespace App\Controller;

use App\Services\JsRenderer\JsRenderer;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    /**
     * TODO: add http method for all action
     * @Route("/", options={"expose"=true})
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * TODO: add http method for all action
     * @Route("/listing", options={"expose"=true})
     */
    public function listing(JsRenderer $jsRenderer)
    {

        $vehicles = json_decode('[
            {"brand": "Audi", "model": "A6", "mec": "2017-01-01", "price": 15000},
            {"brand": "Audi", "model": "A3", "price": 3000, "mec": "02/03/2012"}
          ]',true);
        $jsRenderer->setInitialState(['vehicles'=> $vehicles]);

        return $this->render('home/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);

    }

}
