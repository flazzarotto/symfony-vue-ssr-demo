<?php
/**
 * Created by PhpStorm.
 * User: flazzarotto
 * Date: 15/03/18
 * Time: 11:09
 */

namespace App\Services\JsRenderer;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class JsRenderer
 * @package App\Service
 * Usage :  - In controller, $jsRenderer->setInitialState(['vehicles'=>$data],true|false)
 *              to populate the store state
 *          - In template : {{- js_renderer|raw -}} in your Vue app container
 *          - Also in template, scripts block (can be called on base.html.twig) : {{ js_renderer.initialState|raw }}
 */
class JsRenderer
{

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var string
     */
    private $initialState = null;
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;
    /**
     * @var string current locale
     */
    private $locale = false;

    public function __construct(LoggerInterface $logger, RequestStack $requestStack)
    {
        $this->logger = $logger;
        $this->request = $requestStack->getCurrentRequest();
        if ($this->request !== null) {
            $this->locale = $this->request->getLocale();
        }
    }

    /**
     * Main method - execute and compile Vue App while populating the state store
     * (if already given with setInitialState)
     * TODO add try {} catch {} block to avoid compilation errors (500)
     * @return string compiled HTML
     */
    public function render()
    {
        $request = $this->request;

        $path = $request->getRequestUri();

        $renderer_source = file_get_contents(__DIR__ . '/../../../node_modules/vue-server-renderer/basic.js');
        $app_source = file_get_contents(__DIR__ . '/../../../public/build/js/server.js');
        $v8 = new \V8Js();
        ob_start();
        $v8->executeString('var process = { env: { VUE_ENV: "server", NODE_ENV: "production" }}; this.global = { process: process };');
        $v8->executeString('let url = \'' . $path . '\'; ');
        $v8->executeString($renderer_source);

        if ($this->locale) {
            if ($this->initialState !== null) {
                $v8->executeString('const Vuex_initialState = ' . $this->initialState);
            }

            $v8->executeString('const locale = "' . $this->locale . '"');
        }

        $v8->compileString($app_source);

        $v8->executeString($app_source);
        return ob_get_clean();
    }

    /**
     * Populates the state store client-side
     * TODO: find a good way not to have JSON in page
     */
    public function initialState()
    {
        $state = '<script>';

        if ($this->locale) {
            $state .= 'window.locale = "' . $this->locale . '"';
        }
        if ($this->initialState !== null) {
            $state .= '; window.__INITIAL_STATE__ = ' . $this->initialState;
        }
        $state .= '</script>';
        return $state;
    }

    /**
     * @param array|string $initialState data to store in state - must be associative array or JSON
     * @param bool $isJSON
     */
    public function setInitialState($initialState)
    {
        $this->initialState = json_encode($initialState, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Shortcut for render() method
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

}

