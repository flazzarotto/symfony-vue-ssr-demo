# Information d'installation d'un poste de développement

### Pré-requis
* php 7.1.3
* composer
* mysql 5.7
* Node.js / npm (yarn conseillé)
* V8
* pecl
* V8JS

## Documentation - Sommaire

1. VueJS 2

    1. [Utilisation du SSR](./doc/vue-ssr.md)
    1. [Traduction](./doc/vue-trans.md)
    