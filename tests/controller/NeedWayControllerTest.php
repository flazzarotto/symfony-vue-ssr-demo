<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NeedWayControllerTest extends WebTestCase
{
    public function testWay(): void
    {
        $client = static::createClient();
        $client->request('GET', '/parcours');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('way', $client->getRequest()->attributes->get('_route'));
    }
}
