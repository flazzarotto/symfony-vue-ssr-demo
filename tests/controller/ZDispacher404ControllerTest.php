<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ZDispacher404ControllerTest extends WebTestCase
{
    /**
     * @dataProvider wrongValueTwoArgsData
     */
    public function testwrongValueTwoArgs(string $region, string $second): void
    {
        $client = static::createClient();
        $client->request('GET', '/' . $region . '/' . $second);

        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function wrongValueTwoArgsData(): array
    {
        return [
            ['badregion', 'seine-et-marne'],
            ['ile-de-france', 'baddepart'],
            ['badregion', 'audi'],
            ['paca', 'badbrand']
        ];
    }

    /**
     * @dataProvider wrongValueThreeArgsData
     */
    public function testwrongValueThreeArgs(string $region, string $second, string $third): void
    {
        $client = static::createClient();
        $client->request('GET', '/' . $region . '/' . $second . '/' . $third);

        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function wrongValueThreeArgsData(): array
    {
        return [
            ['badregion', 'seine-et-marne', 'melun'],
            ['ile-de-france', 'baddepart', 'melun'],
            ['ile-de-france', 'seine-et-marne', 'badcity'],
            ['badregion', 'var', 'audi'],
            ['paca', 'baddepart', 'audi'],
            ['paca', 'var', 'badbrand'],
            ['badregion', 'audi', 'a1'],
            ['paca', 'badbrand', 'a1'],
            ['paca', 'audi', 'badmodel']
        ];
    }

    /**
     * @dataProvider wrongValueFourthArgsData
     */
    public function testwrongValueFourArgs(string $region, string $second, string $third, string $fourth): void
    {
        $client = static::createClient();
        $client->request('GET', '/' . $region . '/' . $second . '/' . $third . '/' . $fourth);

        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function wrongValueFourthArgsData(): array
    {
        return [
            ['badregion', 'seine-et-marne', 'melun', 'audi'],
            ['ile-de-france', 'baddepart', 'melun', 'audi'],
            ['ile-de-france', 'seine-et-marne', 'badcity', 'audi'],
            ['ile-de-france', 'seine-et-marne', 'melun', 'badbrand'],
            ['badregion', 'seine-et-marne', 'audi', 'a1'],
            ['ile-de-france', 'baddepart', 'audi', 'a1'],
            ['ile-de-france', 'seine-et-marne', 'badbrand', 'a1'],
            ['ile-de-france', 'seine-et-marne', 'audi', 'badmodel']
        ];
    }

    /**
     * @dataProvider wrongValueFiveArgsData
     */
    public function testwrongValueFiveArgs(string $region, string $depart, string $city, string $brand, string $model)
    {
        $client = static::createClient();
        $client->request('GET', '/' . $region . '/' . $depart . '/' . $city . '/' . $brand . '/' . $model);

        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function wrongValueFiveArgsData(): array
    {
        return [
            ['badregion','seine-et-marne','melun','audi','a1'],
            ['ile-de-france','baddepart','melun','audi','a1'],
            ['ile-de-france','seine-et-marne','badcity','audi','a1'],
            ['ile-de-france','seine-et-marne','melun','badbrand','a1'],
            ['ile-de-france','seine-et-marne','melun','audi','badmodel']
        ];
    }
}
