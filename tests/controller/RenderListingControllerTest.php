<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RenderListingControllerTest extends WebTestCase
{
    public function testListing(): void
    {
        $client = static::createClient();
        $client->request('GET', '/auto');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('listing', $client->getRequest()->attributes->get('_route'));
    }

    /**
     * @dataProvider goodValueBrandData
     */
    public function testBrand(string $brand): void
    {
        $client = static::createClient();
        $client->request('GET', '/auto/' . $brand);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function goodValueBrandData(): array
    {
        return [
            ['renault'],
            ['audi']
        ];
    }

    /**
     * @dataProvider goodValueModelData
     */
    public function testModel(string $brand, string $model): void
    {
        $client = static::createClient();
        $client->request('GET', '/auto/' . $brand . '/' . $model);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function goodValueModelData(): array
    {
        return [
            ['renault','clio'],
            ['audi', 'a1']
        ];
    }

    /**
     * @dataProvider goodValueRegionData
     */
    public function testRegion(string $region): void
    {
        $client = static::createClient();
        $client->request('GET', '/' . $region);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function goodValueRegionData(): array
    {
        return [
            ['ile-de-france'],
            ['paca']
        ];
    }
}
