<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RenderListing404ControllerTest extends WebTestCase
{
    /**
     * @dataProvider wrongValueBrandData
     */
    public function testBrand(string $brand): void
    {
        $client = static::createClient();
        $client->request('GET', '/auto/' . $brand);

        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function wrongValueBrandData(): array
    {
        return [
            ['badbrand']
        ];
    }

    /**
     * @dataProvider wrongValueModelData
     */
    public function testModel(string $brand, string $model): void
    {
        $client = static::createClient();
        $client->request('GET', '/auto/' . $brand . '/' . $model);

        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function wrongValueModelData(): array
    {
        return [
            ['badbrand','clio'],
            ['audi', 'badmodel']
        ];
    }

    /**
     * @dataProvider wrongValueRegionData
     */
    public function testRegion(string $region): void
    {
        $client = static::createClient();
        $client->request('GET', '/' . $region);

        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function wrongValueRegionData()
    {
        return [
            ['badregiob']
        ];
    }
}
