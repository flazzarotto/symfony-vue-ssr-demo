<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ZDispacherControllerTest extends WebTestCase
{
    /**
     * @dataProvider goodValueTwoArgsData
     */
    public function testGoodValueTwoArgs(string $region, string $second): void
    {
        $client = static::createClient();
        $client->request('GET', '/' . $region . '/' . $second);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function goodValueTwoArgsData()
    {
        return [
            ['ile-de-france', 'seine-et-marne'],
            ['paca', 'var'],
            ['paca', 'audi'],
            ['ile-de-france', 'renault'],
        ];
    }

    /**
     * @dataProvider goodValueThreeArgsData
     */
    public function testGoodValueThreeArgs(string $region, string $second, string $third): void
    {
        $client = static::createClient();
        $client->request('GET', '/' . $region . '/' . $second . '/' . $third);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function goodValueThreeArgsData()
    {
        return [
            ['ile-de-france', 'seine-et-marne', 'melun'],
            ['ile-de-france', 'seine-et-marne', 'lagny-sur-marne'],
            ['paca', 'var', 'audi'],
            ['paca', 'var', 'renault'],
            ['paca', 'audi', 'a1'],
            ['paca', 'renault', 'clio']
        ];
    }

    /**
     * @dataProvider goodValueFourthArgsData
     */
    public function testGoodValueFourArgs(string $region, string $second, string $third, string $fourth): void
    {
        $client = static::createClient();
        $client->request('GET', '/' . $region . '/' . $second . '/' . $third . '/' . $fourth);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function goodValueFourthArgsData()
    {
        return [
            ['ile-de-france', 'seine-et-marne', 'melun', 'audi'],
            ['ile-de-france', 'seine-et-marne', 'audi', 'a1'],
            ['paca', 'var', 'la-seyne-sur-mer', 'renault'],
            ['paca', 'var', 'audi', 'a1'],
        ];
    }

    /**
     * @dataProvider goodValueFiveArgsData
     */
    public function testGoodValueFiveArgs(string $region, string $depart, string $city, string $brand, string $model)
    {
        $client = static::createClient();
        $client->request('GET', '/' . $region . '/' . $depart . '/' . $city . '/' . $brand . '/' . $model);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function goodValueFiveArgsData()
    {
        return [
            ['ile-de-france','seine-et-marne','melun','audi','a1'],
            ['ile-de-france','seine-et-marne','lagny-sur-marne','renault','clio']
        ];
    }
}
