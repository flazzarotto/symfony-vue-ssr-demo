<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WayControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/parcours');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('pa_vo_way', $client->getRequest()->attributes->get('_route'));
    }
}
