<?php

namespace App\Tests\Services\Dispacher;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Services\Dispatcher\Dispatcher;

class DispacherControllerTest extends KernelTestCase
{

    public function testHashContains(): void
    {
        $dataAccessor = $this->getMockBuilder('\App\Services\DataAccessor\ParameterAccessor')
            ->disableOriginalConstructor()
            ->getMock();
        $dataAccessor
            ->method('valueExists')
            ->willReturn(true);
        $dispacher = new Dispatcher($dataAccessor);

        $result = $dispacher->getGeoRouteArgs(['test', 'test']);

        $this->assertArrayHasKey('region', $result);
        $this->assertArrayHasKey('department', $result);
        $this->assertArrayHasKey('brand', $result);
        $this->assertArrayHasKey('model', $result);
        $this->assertArrayHasKey('city', $result);
        $this->assertEquals($result['region'], 'test');
    }

    public function testWrongValue(): void
    {
        $dataAccessor = $this->getMockBuilder('\App\Services\DataAccessor\ParameterAccessor')
            ->disableOriginalConstructor()
            ->getMock();
        $dataAccessor
            ->method('valueExists')
            ->willReturn(false);
        $dispacher = new Dispatcher($dataAccessor);

        $this->expectException('\Symfony\Component\HttpKernel\Exception\NotFoundHttpException');

        $dispacher->getGeoRouteArgs(['test', 'test']);
    }
}
