<?php

namespace App\Tests\Services\DataAccessor;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Services\DataAccessor\ParameterAccessor;

class ParameterAccessorTest extends KernelTestCase
{
    private $container;
    private $location;
    private $brands;
    private $dataAccessor;

    public function setUp(): void
    {
        self::bootKernel();
        $this->container = self::$kernel->getContainer();
        $this->location = $this->container->getParameter('location');
        $this->brands = $this->container->getParameter('brands');
        $this->dataAccessor = new ParameterAccessor($this->location, $this->brands);
    }

    /**
     * @dataProvider typesDataForValueExists
     */
    public function testValueExists(string $type, string $value, array $parents): void
    {
        $this->assertTrue($this->dataAccessor->valueExists($type, $value, $parents));
    }

    public function typesDataForValueExists(): array
    {
        return [
            ['region', 'ile-de-france', []],
            ['department', 'seine-et-marne', ['ile-de-france']],
            ['brand', 'audi', []],
            ['model', 'a1', ['audi']],
            ['city', 'melun', ['ile-de-france', 'seine-et-marne']]
        ];
    }

    /**
     * @dataProvider typesDataForValueNotExists
     */
    public function testValueNotExists(string $type, string $value, array $parents): void
    {
        $this->assertFalse($this->dataAccessor->valueExists($type, $value, $parents));
    }

    public function typesDataForValueNotExists(): array
    {
        return [
            ['region', 'audi', []],
            ['department', 'poitiers', ['ile-de-france']],
            ['brand', 'seine-et-marne', []],
            ['model', 'a1', ['', 'renault']],
            ['city', 'marseille', ['ile-de-france', 'seine-et-marne']]
        ];
    }
}
