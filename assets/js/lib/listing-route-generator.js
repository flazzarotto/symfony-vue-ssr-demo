import {objectFilter} from './lib'

export default function ($router, actualParams, onComplete = null, onAbort = null) {

    // avoid state modification
    const allParams = {...actualParams}

    const params = {}

    let query = {}

    let name = ''

    const mapper = [
        {
            name: 'app_zdispatcher_dispatchfiveargs',
            params: [
                'region',
                'depart',
                'city',
                'brand',
                'model'
            ]
        },
        {
            name: 'app_zdispatcher_dispatchfourargs',
            params: [
                'region',
                'depart',
                'city',
                'brand'
            ]
        },
        {
            name: 'app_zdispatcher_dispatchfourargs',
            params: [
                'region',
                'depart',
                'city',
                'brand'
            ]
        },
        {
            name: 'app_zdispatcher_dispatchthreeargs',
            params: [
                'region',
                'depart',
                'brand'
            ]
        },
        {
            name: 'app_zdispatcher_dispatchthreeargs',
            params: [
                'region',
                'depart',
                'city'
            ]
        },
        {
            name: 'app_zdispatcher_dispatchthreeargs',
            params: [
                'region',
                'brand',
                'model'
            ]
        },
        {
            name: 'app_zdispatcher_dispatchtwoargs',
            params: [
                'region',
                'depart'
            ]
        },
        {
            name: 'app_zdispatcher_dispatchtwoargs',
            params: [
                'region',
                'brand'
            ]
        },
        {
            name: 'app_renderlisting_region',
            params: [
                'region'
            ]
        },
        {
            name: 'app_renderlisting_model',
            params: [
                'brand',
                'model'
            ]
        },
        {
            name: 'app_renderlisting_brand',
            params: [
                'brand'
            ]
        },
        {
            name: 'app_renderlisting_listing',
            params: []
        }
    ]

    for (let i in mapper) {
        if (!mapper.hasOwnProperty(i)) {
            continue
        }
        let all = true
        for (let j in mapper[i].params) {
            if (!mapper[i].params.hasOwnProperty(j)) {
                continue
            }
            let param = mapper[i].params[j]
            if (allParams[param] === undefined || allParams[param] === null) {
                all = false
                break
            }
        }
        if (all) {
            name = mapper[i].name
            for (let j in mapper[i].params) {
                if (!mapper[i].params.hasOwnProperty(j)) {
                    continue
                }
                let param = mapper[i].params[j]
                params[param] = allParams[param]
                delete allParams[param]
            }
            query = objectFilter(allParams, param => param !== null)

            if (query.price) {
                query['price[min]'] = query.price.min
                query['price[max]'] = query.price.max
                delete query.price
            }

            break
        }
    }

    if (name.length) {
        $router.push({name, params, query}, onComplete, onAbort)
    }

}