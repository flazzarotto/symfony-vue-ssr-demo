import Vue from 'vue'
import Vuex from 'vuex'
import vehiclestore from './stores/vehiclestore'

Vue.use(Vuex)

let store = {
    state: {},
    getters: {},
    mutations: {},
    actions: {}
}

// store the 2 stores in the store
for (let i in store) {
    store[i] = {...{...store[i], ...vehiclestore[i]}}
}

export default new Vuex.Store(store)
