import Vue from 'vue'
import Router from 'vue-router'

import Listing from '../Listing'
import Home from '../Home'

import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js'
import generatedRoutes from '../data/routes.json'

Vue.use(Router)

Routing.setRoutingData(generatedRoutes)

const routes = []

function componentMatch(routeName) {
    if (routeName === 'app_default_index') {
        return Home
    }
    if (routeName.indexOf('app_default_listing') === 0) {
        return Listing
    }
    return null
}

for (let routeName in generatedRoutes.routes) {
    if (!generatedRoutes.routes.hasOwnProperty(routeName)) {
        continue
    }
    let route = generatedRoutes.routes[routeName]

    let parameters = {}

    for (let i in route.tokens) {
        if (!route.tokens.hasOwnProperty(i)) {
            continue
        }
        let token = route.tokens[i]
        if (token[0] === 'variable') {
            parameters[token[3]] = '---' + token[3]
        }
    }

    routes.push(
        {
            path: Routing.generate(routeName, parameters).replace(/---/g, ':'),
            component: componentMatch(routeName),
            name: routeName
        }
    )
}

export default new Router({
    mode: 'history',
    routes: routes,
})

