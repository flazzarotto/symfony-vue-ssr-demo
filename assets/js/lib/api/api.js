import axios from 'axios'

const Api = {

    getVehicles(listNumber, success, error) {
        axios.get('http://localhost:3000/'+listNumber).then(response => {
            success(response)
        }, response => {
            error(response)
        })
    }

}

export default Api