// TODO finish implementing this

import axios from 'axios'
import httpAdapter from 'axios/lib/adapters/http'
// import { stringify } from 'qs'
// import Cookies from 'js-cookie'

// import { jwtBearerName } from '../actions/auth'
// import { store } from '../store' // redux
import store from '../store'

export class $http {
    static ajax (method, url, args = {}, userParams) {

        const params = {
            url,
            method,
            paramsSerializer: function (params) {
                return stringify(params, { arrayFormat: 'brackets', encode: false })
            },
            headers: {}
        }

        const state = store.getters.getAuthParameters() // vuex
        const lang = state.SETTING.locale
        const token = Cookies.get(jwtBearerName)
        const contentType = userParams.contentType

        if (contentType) {
            params.headers['Content-Type'] = contentType
        }

        if (lang) {
            params.headers['Accept-Language'] = lang
        }

        if (token) {
            params.headers['Authorization'] = 'Bearer ' + token
        }

        if (['post', 'pull'].indexOf(method) > -1) {
            if (contentType && contentType === 'multipart/form-data') {
                params.data = args
            } else {
                params.data = { ...args }
            }
        } else {
            params.params = { ...args }
        }

        if (process.env.NODE_ENV === 'test') {
            axios.defaults.adapter = httpAdapter
        }

        return axios(params)
            .then(res => res.data)
            .catch(error => {
                console.error('BOPA:$http:ajax:catch', error)
                if (error.response) {
                    return Promise.reject(error.response.data)
                }

                return Promise.reject(error)
            })
    }

    static get (url, args, params = {}) {
        return $http.ajax('get', url, args, params)
    }

    static post (url, args, params = {}) {
        return $http.ajax('post', url, args, params)
    }

    static put (url, args, params = {}) {
        return $http.ajax('put', url, args, params)
    }

    static delete (url, args, params = {}) {
        return $http.ajax('delete', url, args, params)
    }
}

