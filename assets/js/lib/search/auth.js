import { $http } from '../services/http'
import { apiUri } from '../../../config/app.config'
import Cookies from 'js-cookie'
import jwtDecode from 'jwt-decode'

import * as ActionTypes from '../constants/ActionTypes'

export let TIMER_REFRESH_TOKEN
const TTL_REFRESH_TOKEN = 1000 * 60 // milliseconds

export const jwtRefreshTokenName = 'jwtTokenRefresh'
export const jwtBearerName = 'jwtBearer'

export function userLoginRequest (args) {
    return {
        type: ActionTypes.AUTH_USER_PENDING,
        payload: {
            ...args
        }
    }
}

export function userLoginSuccess (data) {
    return {
        type: ActionTypes.AUTH_USER_SUCCESS,
        payload: {
            ...data
        }
    }
}

export function userLoginFailed (error) {
    return {
        type: ActionTypes.AUTH_USER_FAILED,
        payload: new Error(ActionTypes.AUTH_USER_FAILED + error),
        error: ActionTypes.AUTH_USER_FAILED
    }
}

export function updateUserTokenRequest (args) {
    return {
        type: ActionTypes.AUTH_REFRESH_PENDING,
        payload: {
            ...args
        }
    }
}

export function updateUserTokenSuccess (data) {
    return {
        type: ActionTypes.AUTH_REFRESH_SUCCESS,
        payload: {
            ...data
        }
    }
}

export function updateUserTokenFailed (error) {
    return {
        type: ActionTypes.AUTH_REFRESH_FAILED,
        payload: new Error(ActionTypes.AUTH_REFRESH_FAILED + error),
        error: ActionTypes.AUTH_REFRESH_FAILED
    }
}

export function userLogout () {
    return {
        type: ActionTypes.AUTH_USER_LOGOUT
    }
}



/**
 * Vérifie la validité du token et relance automatique
 * @returns {Function} - Redux dispatch
 */
export function refreshTokenRepeater () {
    return (dispatch, getState) => {
        let tokenBearer = Cookies.get(jwtBearerName)
        if (tokenBearer) {
            let refreshToken = Cookies.get(jwtRefreshTokenName)
            const decodedToken = jwtDecode(tokenBearer)
            if (decodedToken.exp) {
                const expireAt = new Date(decodedToken.exp * 1000).getTime()
                if (expireAt > (new Date(Date.now() + TTL_REFRESH_TOKEN)).getTime()) {
                    const state = getState()
                    dispatch(refreshTokenRepeaterTimer())
                    if (state.auth && state.auth.isAuthenticated) {
                        return Promise.resolve(true)
                    }
                    const payload = {
                        token: tokenBearer,
                        refresh_token: refreshToken
                    }
                    return Promise.resolve(dispatch(updateUserTokenSuccess(payload)))
                }
            }

            const payload = { 'refresh_token': refreshToken }
            dispatch(updateUserTokenRequest(payload))

            return $http.get(apiUri + '/api/token_check', payload)
                .then(json => {
                    dispatch(refreshTokenRepeaterTimer())
                    return Promise.resolve(dispatch(updateUserTokenSuccess(json)))
                })
                .catch((err) => {
                    return Promise.resolve(dispatch(updateUserTokenFailed(err)))
                })
        } else {
            return Promise.resolve(dispatch(updateUserTokenFailed('Token not found')))
        }
    }
}

/**
 * Vérification toutes les (TTL_REFRESH_TOKEN / 3)ms de la validité du token
 */
export function refreshTokenRepeaterTimer () {
    return (dispatch) => {
        clearTimeout(TIMER_REFRESH_TOKEN)
        TIMER_REFRESH_TOKEN = setTimeout(() => dispatch(refreshTokenRepeater()), 10000)
    }
}


