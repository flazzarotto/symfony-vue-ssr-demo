export const objectFilter = (object, predicate) => {
    const result = {}
    let key

    for (key in object) {
        if (object.hasOwnProperty(key) && predicate(object[key])) {
            result[key] = object[key]
        }
    }

    return result
}
