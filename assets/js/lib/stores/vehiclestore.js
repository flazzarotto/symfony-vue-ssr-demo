import listingRouteGenerator from '../../lib/listing-route-generator'
import Api from '../../lib/api/api'
import Vue from 'vue'

export default {
    state: {
        vehicles: [],
    },
    getters: {
        vehicles(state) {
            return state.vehicles
        }
    },
    mutations: {
        updateVehicles(state, vehicles) {
            console.log(vehicles)
            state.vehicles = vehicles
        }
    },
    actions: {
        setCriteria(context, criteria) {
            Api.getVehicles(
                criteria,
                (response) => {
                    this.commit('updateVehicles', response.data)
                },
                (response) => {
                    console.log('error', response)
                }
            )
        }
    }
}
