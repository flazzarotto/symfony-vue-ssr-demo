export const dateFormat = (value) => {
    const pattern1 = /([0-9]{2})[^0-9]+([0-9]{2})[^0-9]+([0-9]{4})/
    const pattern2 = /([0-9]{4})[^0-9]+([0-9]{2})[^0-9]+([0-9]{2})/

    return value.match(pattern1) ? value.replace(pattern1, '$1/$2/$3') : yearFromDate(value)
    || value.match(pattern2) ? value.replace(pattern2, '$3/$2/$1') : yearFromDate(value)
}

export const yearFromDate = (date) => {
    let year = date.match(/[0-9]{4}/)
    return year.length ? year[0] : date
}