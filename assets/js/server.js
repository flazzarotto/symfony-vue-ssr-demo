import app from './app'
import router from './lib/router'
import store from './lib/store'

/*
 * Replaces state with server pre-fetched data
 */
if ('undefined' !== typeof Vuex_initialState) {
    store.replaceState({...store.state, ...Vuex_initialState})
}

/*
 * Render vue into html code according to routing
 */
new Promise((resolve, reject) => {
    router.push(url)
    router.onReady(() => {
        const matchedComponents = router.getMatchedComponents()
        if (!matchedComponents.length) {
            return reject({code: 404})
        }
        resolve(app)
    }, reject)
})
    .then(app => {
        renderVueComponentToString(app, (err, res) => {
            print(res)
        })
    })
    .catch((err) => {
        print(err)
    })


