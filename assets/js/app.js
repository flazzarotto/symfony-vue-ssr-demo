// dependencies
import Vue from 'vue'
import VueLodash from 'vue-lodash'
import VueI18n from 'vue-i18n'

// local
import App from './App.vue'
import router from './lib/router'
import store from './lib/store'
import messages from './data/translations.json'

Vue.use(VueI18n)
Vue.use(VueLodash)

Vue.mixin({
    beforeMount() {
        const {asyncData} = this.$options
        if (asyncData) {
            this.dataPromise = asyncData({
                store: this.$store,
                route: this.$route
            })
        }
    }
})

Vue.filter('capitalize', function (value) {
    if (!value) {
        return ''
    }
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
})

const i18n = new VueI18n({locale, fallbackLocale: 'en', messages, silentTranslationWarn: true})

Vue.filter('trans', function (key, choice = null, vars = {}) {

    if (choice instanceof Object) {
        vars = choice
        choice = vars.count !== undefined ? vars.count : null
    }
    else if (choice && !Object.keys(vars).length) {
        vars.count = choice
    }

    // test trans vs transchoice
    let trans = choice ? '|' : i18n.t(key, vars)
    if (trans.indexOf('|') < 0) {
        return trans
    }

    if (choice === null) {
        choice = 1
    }

    trans = i18n.tc(key, choice, vars)

    return trans
})

Vue.filter('uppercase', function(value) {
    return value.toUpperCase()
})

export default new Vue({
    router,
    store,
    render: h => h(App),
    i18n
})

