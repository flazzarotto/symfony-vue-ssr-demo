import app from './app'
import router from './lib/router'
import store from './lib/store'
import VueSmoothScroll from 'vue2-smooth-scroll'
import Vue from 'vue'

Vue.use(VueSmoothScroll)

/*
 * Replaces state with server pre-fetched data
 */
if (undefined !== window.__INITIAL_STATE__) {
    store.replaceState({...store.state, ...window.__INITIAL_STATE__})
}

router.onReady(() => {
    app.$mount('#main')
})
