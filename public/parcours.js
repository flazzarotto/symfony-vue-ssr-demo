var brandList = [];

(function () {
    var STEP_SCROLL_SPEED = 600
    var SPRITE_IMAGE_WIDTH = 750

    var SUGGEST_INPUT_MINLENGTH = 1
    var SUGGEST_INPUT_DISPLAY_LIMIT = 10

    var stepList = null

    var brandItemTemplate = null
    var brandSelected = null
    $(document).ready(function () {

        // prise en compte des inputs de type numerique
        if ($.browser.mobile) {
            $("#parcours").find(".numeric-input").attr("type", "number");
        }

        stepList = $("#parcours > div");

        // on restreint la saisie des input[text] au format numerique
        $("#parcours .numeric-input:not(.suggest)")
            .on("keypress", handleIntValue)
            .on("keyup", checkValues)

        // step 1
        $(".step_1 a")
            .click(validateStep)

        // step 2
        $(".step_2 a")
            .hover(handleItemOver, handleItemOut)
            .click(setSingleValue)

        // step 3
        $(".step_3 a")
            .hover(handleItemOver, handleItemOut)
            .click(setSingleValue)

        // step 4
        $(".step_4 a")
            .hover(handleItemOver, handleItemOut)
            .click(setSingleValue)

        // step 5
        $(".step_5 a")
            .click(setSingleValue)

        // step 6
        $(".step_6 a")
            .hover(handleItemOver, handleItemOut)
            .click(setSingleValue)

        // step 7
        $(".step_7 a")
            .click(setSingleValue)

        // step 8
        $(".step_8 .brand-menu .btn_link")
            .click(setSingleValue)

        $(".step_8 .brand-menu a:first")
            .click(showBrandList)

        // init de la liste des marques principales
        initBrandList()

        // init du module de suggest pour le choix de marque
        initBrandSuggest()

        // gestion de l'ajout d'une nouvelle marque
        $(".add-brand").click(addBrand)
        $(".add-brand-overlay .close-add").click(closeBrandOverlay)
        $(".add-brand-validate").on("click", addBrandValidate)

        // selection / deselection d'une marque
        $(".brand-list").on("click", "li:not(.add-brand)", checkBrand)

        // validation de la selection de marque(s)
        $(".step_8 .btn_bottom a")
            .click(validateBrandList)

        // step 9
        $(".step_9 a.btn_link")
            .click(setSingleValue)

        $(".step_9 a.btn-default-parcours")
            .click(setInputsValue)

        // step 10
        $(".step_10 a")
            .hover(handleItemOver, handleItemOut)
            .click(setSingleValue)

        // step 11
        $(".step_11 a.btn-default-parcours")
            .click(setLocalization)

        loadParcoursFormCookie();

        $('#parcours .numeric-input:not(.suggest)').each(function () {
            validateInputText($(this));
        });

    })

    function handleIntValue(e) {
        var pattern = /^\d+$/

        var key = e.which || e.keyCode
        var char = String.fromCharCode(key)

        var specialKey = (key == 8 || key == 35 || key == 36 || key == 37 || key == 39 || key == 46)

        if (key == 13) {
            var parent = $(this).parents("div[class^='step_']")
            var button = parent.find(".btn-default-parcours")

            if (!button.hasClass("disabled")) {
                button.trigger("click")
            }
        }

        return pattern.test(char) || specialKey
    }

    function checkValues(e) {
        validateInputText($(this));
    }

    function validateInputText(inputText) {
        var parent = $(inputText.parents("[data-property-name]")[0])
        var propertyName = parent.data("property-name")
        var propertyType = parent.data("property-type")
        var button = parent.find(".btn-default-parcours")

        var inputs = parent.find(".numeric-input")

        var disabled = true
        if (inputs.length == 2) {
            var val1 = inputs[0].value
            var val2 = inputs[1].value

            disabled = !(val1 > 0 || val2 > 0)

            // verification des valeurs min/max
            if (!disabled && propertyType == "minMax") {
                var span = $(inputs[0]).parent()

                disabled = (parseInt(val1) >= parseInt(val2))

                if (disabled) {
                    if (!span.hasClass("error")) {
                        span.addClass("error")
                    }
                }
                else {
                    span.removeClass("error");
                    if (propertyName === "budget") {
                        $("#linkBudgetPi").removeClass("active");
                    }
                }
            }
        }
        if (inputs.length === 1 && propertyName === "localisation") {
            var location = inputs[0].value;
            if ($("#linkLocationPi").hasClass("active")) {
                disabled = false;
                if (location > 0 && (location.length === 2 || location.length === 5))
                    $("#linkLocationPi").removeClass("active");
            } else {
                disabled = !(location > 0 && (location.length === 2 || location.length === 5));
                if (!disabled)
                    $("#linkLocationPi").removeClass("active");
            }
        }

        if (disabled) {
            if (!button.hasClass("disabled")) {
                button.addClass("disabled")
            }
        }
        else {
            button.removeClass("disabled")
        }
    }

    function handleItemOver() {
        if (!$(this).hasClass("btn_link")) {
            setSpritePosition($(this))
        }
    }

    function handleItemOut() {
        if (!$(this).hasClass("btn_link")) {
            var selection = $(this).parents(".selection")
            var image = selection.children("div[id^=image]")

            image.children("div")
                .removeClass("active")
        }
    }

    function setSpritePosition(item, isValue) {
        var selection = item.parents(".selection")
        var image = selection.children("div[id^=image]")

        if (image.length == 1) {
            var currentItem = item.parent("li")
            var items = currentItem.parent("ul").children("li")

            var index = items.index(currentItem) + 1

            // l'utilisateur a clique sur "peu importe"
            if (item.hasClass("btn_link")) {
                index = 0
            }

            if (isValue == undefined) {
                image = image.children("div")
            }

            image
                .css("background-position", -SPRITE_IMAGE_WIDTH * index)
                .addClass("active")
        }
    }

    function setSingleValue(e) {
        //alert("setSingleValue");
        var selected = $(e.target);
        var currentStep = selected.parents("div[class^='step_']");
        $('.popContent:first').text();
        setSingleData(selected);
        validateStep(e, currentStep);
        //alert(currentStep.next("div[class^='step_']").find(".title:first").text());
        trackingOccasion(this, 'Occasion', 'Tunnel', currentStep.next("div[class^='step_']").find(".title:first").text());
    }

    function setSingleData(selected) {
        if (!selected.is("a")) {
            selected = selected.parent("a")
        }

        setSpritePosition(selected, true)

        var currentStep = selected.parents("div[class^='step_']")
        var currentValue = selected.data("item-value")

        currentStep.find("a").removeClass("active")
        currentStep.attr("data-property-value", currentValue)

        selected.addClass("active")



        if (currentStep.hasClass("step_2")) {
            var selectedId = selected.attr('id');
            switch (selectedId) {
                // Si choix des sportives et luxueuses
                // Désactiver les réponses « 6 personnes » et « 7+ » pour la question nombre de personnes
                // Désactiver la réponse « très grand » pour la question taille du coffre
                case "linkSportLux":
                    $("#link6Per").addClass("disabled");
                    $("#link7Per").addClass("disabled")
                    $("#linkTrunkSizeVeryBig").addClass("disabled")
                    $("#linkDrivingPositionLow").removeClass("disabled");
                    break;

                // Si choix des Ecologiques
                // Désactiver la réponses « position basse » pour la question position de conduite
                case "linkEcolo":
                    $("#link6Per").removeClass("disabled");
                    $("#link7Per").removeClass("disabled");
                    $("#linkTrunkSizeVeryBig").removeClass("disabled")
                    $("#linkDrivingPositionLow").addClass("disabled");
                    break;
                default:
                    $("#link6Per").removeClass("disabled");
                    $("#link7Per").removeClass("disabled")
                    $("#linkTrunkSizeVeryBig").removeClass("disabled")
                    $("#linkDrivingPositionLow").removeClass("disabled");
            }
        }

        // Désactiver les positions basses pour une distance = grande
        if (currentStep.hasClass("step_5")) {
            var selectedId = selected.attr('id');
            if (selectedId == "linkDistanceBig")
                $("#linkDrivingPositionLow").addClass("disabled");
            else
                $("#linkDrivingPositionLow").removeClass("disabled");
        }

        // Si on clique sur le boutton "Peu Importe" du Block de budget, on vide le champs: budget min et max.
        if (selected.attr('id') === "linkBudgetPi") {
            $("#txtBudgetMin").val("");
            $("#txtBudgetMax").val("");
        }

        // Si on clique sur le boutton "Peu Importe" du Block des marques, on doit désélectionner les anciennes marques.
        if (selected.attr('id') === "linkBrandPi") {
            for (var i = 0; i < brandList.length; i++) {
                var brand = $(".step_8").find("[data-brand-name='" + brandList[i] + "']");
                var check = brand.find("input");
                var isChecked = check.prop("checked");
                check.prop("checked", !isChecked);
                brand.toggleClass("valid");
            }
            brandList = [];
        }
    }



    // Récupérer les valeurs du parcours depuis la cookie et charger tous les blocks avec les bonnes réponses.
    function loadParcoursFormCookie() {
        var parcours = $.cookie('parcours');
        if (parcours != null) {
            var parcoursViewModel = JSON.parse(parcours);

            if (parcoursViewModel.CarBehaviour) {
                setSingleData($(".step_2").find("[data-item-value='" + parcoursViewModel.CarBehaviour + "']"));
                $(".step_2").addClass("active");
            }

            if (parcoursViewModel.PersonsNb) {
                setSingleData($(".step_3").find("[data-item-value='" + parcoursViewModel.PersonsNb + "']"));
                $(".step_3").addClass("active");
            }

            if (parcoursViewModel.TrunkSize) {
                setSingleData($(".step_4").find("[data-item-value='" + parcoursViewModel.TrunkSize + "']"));
                $(".step_4").addClass("active");
            }

            if (parcoursViewModel.Distance) {
                setSingleData($(".step_5").find("[data-item-value='" + parcoursViewModel.Distance + "']"));
                $(".step_5").addClass("active");
            }

            if (parcoursViewModel.DrivingPosition) {
                setSingleData($(".step_6").find("[data-item-value='" + parcoursViewModel.DrivingPosition + "']"));
                $(".step_6").addClass("active");
            }

            if (parcoursViewModel.Parking) {
                setSingleData($(".step_7").find("[data-item-value='" + parcoursViewModel.Parking + "']"));
                $(".step_7").addClass("active");
            }


            if (parcoursViewModel.BrandPreference) {
                if (parcoursViewModel.BrandPreference[0] === "all")
                    setSingleData($(".step_8").find("[data-item-value='all']"));
                else {

                    var otherSelectedBrand = parcoursViewModel.BrandPreference;
                    otherSelectedBrand = otherSelectedBrand.filter(function (el) {
                        return mainBrands.indexOf(el) < 0;
                    });

                    var i;
                    if (otherSelectedBrand.length != 0) {
                        var html = $("#brand-item-template").html();
                        brandItemTemplate = Handlebars.compile(html);
                        for (i = 0; i < otherSelectedBrand.length; i++) {
                            var brandName = otherSelectedBrand[i];
                            var imgName = brandName.replace(" ", "-").toLowerCase() + ".png";
                            var brandHtml = brandItemTemplate({ imgName: imgName, brandName: brandName });
                            $(brandHtml).prependTo($(".brand-list"));
                        }
                    }
                    showBrandBlock();
                    for (i = 0; i < parcoursViewModel.BrandPreference.length; i++) {
                        checkSelectedBrand(parcoursViewModel.BrandPreference[i]);
                    }
                }
                $(".step_8").addClass("active");
            }

            if (parcoursViewModel.Budget) {
                if (parcoursViewModel.Budget.length === 2) {
                    if (parcoursViewModel.Budget[0] === 0 && parcoursViewModel.Budget[1] === 0) {
                        var strBudget = "[" + parcoursViewModel.Budget.join(",") + "]";
                        setSingleData($(".step_9").find("[data-item-value='" + strBudget + "']"));
                    }
                    else
                        setInputsData($(".step_9"), parcoursViewModel.Budget);
                }
                $(".step_9").addClass("active");
            }

            if (parcoursViewModel.Year) {
                setSingleData($(".step_10").find("[data-item-value='" + parcoursViewModel.Year + "']"));
                $(".step_10").addClass("active");
            }

            if (parcoursViewModel.Localisation) {
                if (parcoursViewModel.Localisation.ZipCode) {
                    $("#txtZipCode").val(parcoursViewModel.Localisation.ZipCode);
                    if (parcoursViewModel.Localisation.MaximumDistance)
                        $("#lstDistance").val(parcoursViewModel.Localisation.MaximumDistance);
                }
                else
                    setLinkLocation(false);


                $(".step_11").addClass("active");
            }
        }
    }

    function setInputsValue(e) {
        var currentStep = $(e.target).parents("div[class^='step_']");
        var propertyName = currentStep.data("property-name");

        var input1 = currentStep.find("input[name='" + propertyName + "[0]']");
        var input2 = currentStep.find("input[name='" + propertyName + "[1]']");

        var val1 = (input1.val() == "") ? 0 : input1.val();
        var val2 = (input2.val() == "") ? 0 : input2.val();

        //var currentValue = "[" + val1 + "," + val2 + "]"
        var currentValue = val1 + "," + val2;

        currentStep.attr("data-property-value", currentValue);
        validateStep(e, currentStep);
        trackingOccasion(this, 'Occasion', 'Tunnel', currentStep.next("div[class^='step_']").find(".title:first").text());
    }

    function setInputsData(currentStep, tab) {
        var propertyName = currentStep.data("property-name");

        var input1 = currentStep.find("input[name='" + propertyName + "[0]']");
        var input2 = currentStep.find("input[name='" + propertyName + "[1]']");

        input1.val(tab[0] === 0 ? "" : tab[0]);
        input2.val(tab[1] === 0 ? "" : tab[1]);
        //var currentValue = "[" + val1 + "," + val2 + "]"
        var currentValue = tab.join(",");

        currentStep.attr("data-property-value", currentValue);
    }

    function initBrandList() {
        var html = $("#brand-item-template").html()

        brandItemTemplate = Handlebars.compile(html)
        for (var i = 0; i < mainBrands.length; i++) {
            var brandName = mainBrands[i];
            var imgName = brandName.toLowerCase() + ".png"
            var brandHtml = brandItemTemplate({ imgName: imgName, brandName: brandName })

            $(brandHtml).prependTo($(".brand-list"))
        }
    }

    function initBrandSuggest() {
        var input = $(".add-brand-input input[name=brand]")

        var options =
            {
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            }

        options.local = $.map(otherBrands, function (item) {
            return { value: item }
        })

        var suggest = new Bloodhound(options)

        input.typeahead(
            {
                hint: true,
                highlight: true,
                minLength: SUGGEST_INPUT_MINLENGTH
            },
            {
                name: "value",
                displayKey: "value",
                source: suggest.ttAdapter(),
                limit: SUGGEST_INPUT_DISPLAY_LIMIT,
                templates:
                {
                    empty:
                    [
                        "<div class='no-suggest'>",
                        "aucune suggestion trouvée",
                        "</div>"
                    ].join("\n")
                }
            })
            .on("typeahead:autocompleted typeahead:selected", function (e, data) {
                brandSelected = data.value

                addBrandValidate()
            })
    }

    function addBrand(e) {
        e.preventDefault()

        brandSelected = ""

        $(".add-brand-overlay").show()

        // pour fermer la modal a l'appui sur la touche 'esc'
        $(document).keyup(function (e) { if (e.which == 27) closeBrandOverlay(e) })

        // on force le focus sur l'input de type typeahead
        setTimeout(function () { $('.add-brand-input input[name=brand]').focus(); }, 0)
    }

    function closeBrandOverlay(e) {
        e.preventDefault()

        $(".add-brand-overlay").hide()
    }

    function addBrandValidate(e) {
        if (e) {
            e.preventDefault()
        }

        $(".add-brand-overlay").hide()

        var input = $(".add-brand-input input[name=brand]")

        // suppression du plugin "typeahead"
        // => necessaire pour la bonne prise en compte de la derniere liste de marques
        //    (qui ne doit pas integrer la/les marques ajoutee(s) par l'utilisateur)
        input.typeahead("val", "")
        input.typeahead("destroy")

        input.off("typeahead:autocompleted")
        input.off("typeahead:selected")

        // on supprime la marque selectionnee par l'utilisateur
        otherBrands.splice($.inArray(brandSelected, otherBrands), 1)

        // on remet en place le plugin "typeahead"
        initBrandSuggest()

        // on ajoute la marque selectionnee a la liste deja en place
        addBrandToList()
    }

    function addBrandToList() {
        if (brandSelected) {
            var imgName = brandSelected.replace(" ", "-").toLowerCase() + ".png"
            var brandHtml = brandItemTemplate({ imgName: imgName, brandName: brandSelected })

            $(brandHtml)
                .insertBefore($(".add-brand"))
                .trigger("click")
        }
    }

    function checkBrand(e) {
        e.preventDefault()

        var brandName = $(this).data("brand-name")

        var currentStep = $(e.target).parents("div[class^='step_']")
        var button = currentStep.find(".btn-default-parcours")

        var parent = $(e.currentTarget)
        var check = parent.find("input")

        var isChecked = check.prop("checked")

        check.prop("checked", !isChecked)

        if (!isChecked) {
            brandList.push(brandName);
            currentStep.find("a.btn_link:first").removeClass("active");
        }
        else {
            var indexBrandToRemove = brandList.indexOf(brandName);
            if (indexBrandToRemove > -1) {
                brandList.splice(indexBrandToRemove, 1);
            }
            //brandList.splice($.inArray(brandSelected, brandList), 1)
        }

        $(this).toggleClass("valid")

        if (brandList.length == 0 && !button.hasClass("disabled")) {
            button.addClass("disabled")
        }
        else {
            button.removeClass("disabled")
        }
        var currentValue = brandList.join(",");
        currentStep.attr("data-property-value", currentValue);
    }

    function checkSelectedBrand(selectedBrand) {
        var brand = $(".step_8").find("[data-brand-name='" + selectedBrand + "']");

        var currentStep = $(".step_8");
        var button = currentStep.find(".btn-default-parcours");

        var check = brand.find("input")

        var isChecked = check.prop("checked")

        check.prop("checked", !isChecked)

        if (!isChecked) {
            brandList.push(selectedBrand);
        }
        else {
            var indexBrandToRemove = brandList.indexOf(selectedBrand);
            if (indexBrandToRemove > -1) {
                brandList.splice(indexBrandToRemove, 1);
            }
            //brandList.splice($.inArray(brandSelected, brandList), 1);
        }

        brand.toggleClass("valid");

        if (brandList.length == 0 && !button.hasClass("disabled")) {
            button.addClass("disabled");
        }
        else {
            button.removeClass("disabled");
        }
        var currentValue = brandList.join(",");
        currentStep.attr("data-property-value", currentValue);
    }

    function showBrandList(e) {
        e.preventDefault();
        $(".brand-menu li:first").hide();
        $(".step_8 .btn-default-parcours").css("display", "inline-block");
        $(".brand-list").addClass("active");
        var selected = $(e.target)
        var currentStep = selected.parents("div[class^='step_']")
        trackingOccasion(this, 'Occasion', 'Tunnel', currentStep.next("div[class^='step_']").find(".title:first").text());
    }

    function showBrandBlock() {
        $(".brand-menu li:first").hide();
        $(".step_8 .btn-default-parcours").css("display", "inline-block");

        $(".brand-list").addClass("active");
    }

    function validateBrandList(e) {
        var selected = $(e.target)
        var currentStep = selected.parents("div[class^='step_']")
        //var currentValue = "[" + brandList.join(",") + "]"
        var currentValue = brandList.join(",");
        currentStep.attr("data-property-value", currentValue)
        validateStep(e, currentStep)
        trackingOccasion(this, 'Occasion', 'Tunnel', currentStep.next("div[class^='step_']").find(".title:first").text());
    }

    function setLocalization() {

    }

    function postData() {
        var properties = $("[data-property-name]")

        console.log(properties)
    }

    function validateStep(e, currentStep) {
        e.preventDefault()

        var stepIndex = stepList.index(currentStep) + 2

        console.log("step: " + stepIndex)

        var nextStep = $(".step_" + stepIndex)

        $("html,body").animate({ scrollTop: nextStep.offset().top - 30 }, STEP_SCROLL_SPEED, "linear")

        nextStep.addClass("active")
        if (stepIndex == 2)
            trackingOccasion(this, 'Occasion', 'Tunnel', nextStep.find(".title:first").text());
    }
})()


function setLinkLocation(makeTracking) {
    //alert("setLinkLocation");
    var selected = $("#linkLocationPi");
    selected.addClass("active");
    if (selected.attr('id') === "linkLocationPi") {
        $("#txtZipCode").val("");
        $("#lstDistance").val("10");
    }
    var currentStep = selected.parents("div[class^='step_']");
    var button = currentStep.find(".btn-default-parcours");
    button.removeClass("disabled");
    if (makeTracking)
        trackingOccasion(this, 'Occasion', 'Tunnel', currentStep.find(".title:first").text());
}

function ParcoursViewModel(personsNb, trunkSize, distance, parking, drivingPosition, carBehaviour, brandPreference, budget, year, localisation) {
    this.PersonsNb = personsNb,
        this.TrunkSize = trunkSize,
        this.Distance = distance,
        this.Parking = parking,
        this.DrivingPosition = drivingPosition,
        this.CarBehaviour = carBehaviour,
        this.BrandPreference = brandPreference,
        this.Budget = budget,
        this.Year = year,
        this.Localisation = localisation,
        this.Id = ""
}

function LocalisationViewModel(zipCode, maximumDistance) {
    this.ZipCode = zipCode,
        this.MaximumDistance = maximumDistance
}

// Mettre la liste des oids dans la cookie + redirection vers la page de listing des modèles.
function validateQuizz() {
    $(".ajax-loader").show();
    var carBehaviour = $(".step_2").attr("data-property-value");
    var personsNb = parseInt($(".step_3").attr("data-property-value"));
    var trunkSize = $(".step_4").attr("data-property-value");
    var distance = $(".step_5").attr("data-property-value");
    var drivingPosition = $(".step_6").attr("data-property-value");
    var parking = $(".step_7").attr("data-property-value");
    var brandPreferenceList = ["all"];
    var brandPreference = $(".step_8").attr("data-property-value");
    if (brandPreference) {
        brandPreferenceList = brandPreference.split(",");
    }
    var budgetList = [];
    var budgetMin = 0;
    var budgetMax = 0;
    if ($("#txtBudgetMin").val())
        budgetMin = parseInt($("#txtBudgetMin").val());
    if ($("#txtBudgetMax").val())
        budgetMax = parseInt($("#txtBudgetMax").val());

    budgetList.push(budgetMin);
    budgetList.push(budgetMax);

    var year = $(".step_10").attr("data-property-value");

    var localisationEntity;
    if ($("#linkLocationPi").hasClass("active"))
        localisationEntity = new LocalisationViewModel("", 0);
    else {
        var zipCode = $("#txtZipCode").val();
        var maxDistance = $("#lstDistance").val();
        localisationEntity = new LocalisationViewModel(zipCode, maxDistance);
    }

    var parcours = new ParcoursViewModel(personsNb, trunkSize, distance, parking, drivingPosition, carBehaviour, brandPreferenceList, budgetList, year, localisationEntity);
    $.ajax({
        url: '/Parcours/ValidateParcours/',
        type: 'POST',
        contentType: "application/json",
        data: JSON.stringify(parcours),
        success: function (response) {
            if (response.IsSuccess) {
                var parcoursResponse = getParcoursResponse();
                $.cookie('parcoursResponse', JSON.stringify(parcoursResponse), { path: '/' });
                $.cookie('oids', response.Data.OidList, { path: '/' });
                $.cookie('proposedModel', response.Data.MarqueModelList, { path: '/' });
                $.cookie('parcours', JSON.stringify(parcours), { path: '/' });
                $(".ajax-loader").hide();
                parcours.Id = response.Data.Id;
                window.location.href = "/Parcours/GetResult?oids=" +
                    response.Data.OidList +
                    "&parcours=" +
                    JSON.stringify(parcours);
            }
            else {
                $(".ajax-loader").hide();
                alert(response.Message);
            }
        },
        error: function (resultat, statut, erreur) {
            $(".ajax-loader").hide();
        }
    });

}

function checkTrackingLocation() {
    if (!$("#linkLocationPi").hasClass("active"))
        trackingOccasion(this, 'Occasion', 'Tunnel', 'Dans un rayon de');
}

function getParcoursResponse() {
    var carBehaviour = $(".step_2 .selection").find("a.active").text();
    var personsNb = $(".step_3 .selection").find("a.active").text();
    var trunkSize = $(".step_4 .selection").find("a.active").text();
    var distance = $(".step_5 .selection").find("a.active").text();
    var drivingPosition = $(".step_6 .selection").find("a.active").text();
    var parking = $(".step_7 .selection").find("a.active").text();
    var brandPreferenceList = ["all"];
    var brandPreference = $(".step_8").attr("data-property-value");
    if (brandPreference) {
        brandPreferenceList = brandPreference.split(",");
    }
    var budgetList = [];
    var budgetMin = 0;
    var budgetMax = 0;
    if ($("#txtBudgetMin").val())
        budgetMin = parseInt($("#txtBudgetMin").val());
    if ($("#txtBudgetMax").val())
        budgetMax = parseInt($("#txtBudgetMax").val());

    budgetList.push(budgetMin);
    budgetList.push(budgetMax);

    var year = $(".step_10 .selection").find("a.active").text();

    var localisationEntity;
    if ($("#linkLocationPi").hasClass("active"))
        localisationEntity = new LocalisationViewModel("", 0);
    else {
        var zipCode = $("#txtZipCode").val();
        var maxDistance = $("#lstDistance").val();
        localisationEntity = new LocalisationViewModel(zipCode, maxDistance);
    }

    var parcoursResponse = new ParcoursViewModel(personsNb, trunkSize, distance, parking, drivingPosition, carBehaviour, brandPreferenceList, budgetList, year, localisationEntity);
    return parcoursResponse;
}
