const Encore = require('@symfony/webpack-encore')

Encore

    .configureBabel(function(babelConfig) {
        // add additional presets
        babelConfig.presets.push('es2017')
        babelConfig.presets.push('babel-preset-stage-3')
        babelConfig.plugins.push('transform-decorators-legacy')
    })

    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    .addEntry('js/client', './assets/js/client.js')
    .addEntry('js/server', './assets/js/server.js')

    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // uncomment to define the assets of the project
    .addStyleEntry('css/main', './assets/css/main.scss')

    .configureUglifyJsPlugin((options) => {
        options.compress = Encore.isProduction()
        options.beautify = !Encore.isProduction()
    })

    // uncomment if you use Sass/SCSS files
    .enableSassLoader()

    .enableVueLoader()

// uncomment for legacy applications that require $/jQuery as a global variable
// .autoProvidejQuery()


const webpackConfig = Encore.getWebpackConfig()
//
// let CopyWebpackPlugin = require('copy-webpack-plugin')
//
// const copyOptions = {}
//
// webpackConfig.plugins.unshift(
//     new CopyWebpackPlugin([{from: './assets/js/client', to: './js/client'}], copyOptions)
// )
//

module.exports = webpackConfig
