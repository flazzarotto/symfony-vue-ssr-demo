using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;
using MixadSite.Common.Controllers;
using MixadSite.Common.Extensions;
using MixadSite.Search.BusinessObjects;
using MixadSite.Search.Helpers;
using MixadSite.Search.Interfaces;
using MixadSite.Search.Repositories;
using MixadSite.Search.ViewModels;
using MixadSite.Web.Argus.ViewModels;
using Newtonsoft.Json;
using System.Text;
using MixadSite.Web.Argus.Utilities;
using MixadSite.Web.Argus.Helpers;
using MixadSite.Database.Entities;
using MixadSite.Database.DbContexts;
using System.Globalization;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;

namespace MixadSite.Web.Argus.Controllers
{
    public class ParcoursController : BaseController
    {
        #region Properties
        [Ninject.Inject]
        public ISearchRepository SearchRepository { get; set; }
        public readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Public Action Results
        /// <summary>
        /// Afficher la page de parcours
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var parameters = Parameters.SearchItems("parcours");

            // recuperation du code departement a partir de l'ip utilisateur
            var ip = Misc.GetIPAddress();
            var deptCode = "";
            if (!string.IsNullOrEmpty(ip))
            {
                var zipCode = Misc.GetGeolocationZipCode(ip);
                if (!string.IsNullOrEmpty(zipCode))
                {
                    deptCode = zipCode.Substring(0, 2);
                }
            }

            // constitution de la liste principale et secondaire des marques
            var mainBrands = parameters["mainBrands"];
            var mainList = (mainBrands.value as string)
                            .Split(',')
                            .OrderByDescending(x => x)
                            .ToArray();

            var repository = new AutoMarqueModelesRepository();

            var result = SearchRepository.ExecuteQuery<Auto>(@"
                select marque, count(*) as total
                from auto              
                group by marque
                order by marque asc
                limit 0,500");

            //var otherList = (from r in result
            //                 where r.total > 1 && Array.FindIndex(mainList, t => t.Equals(r.marque, StringComparison.InvariantCultureIgnoreCase)) == -1
            //                 select r.marque.Replace(" ", "-").CamelCase()).ToList();

            var otherList = (from r in result
                             where r.total >= 1 && Array.FindIndex(mainList, t => t.Equals(r.marque, StringComparison.InvariantCultureIgnoreCase)) == -1
                             select r.marque.CamelCase()).ToList();

            otherList.RemoveAll(x => repository.ByMarque(x) == null);

            ViewBag.DeptCode = deptCode;
            ViewBag.UserAnswers = JsonConvert.SerializeObject(new ParcoursViewModel());
            ViewBag.MainBrands = JsonConvert.SerializeObject(mainList);
            ViewBag.OtherBrands = JsonConvert.SerializeObject(otherList);

            Session["CurrentParcoursId"] = null;

            if (Request.UrlReferrer != null && (Request.UrlReferrer.AbsoluteUri.EndsWith(".largus.fr/") || Request.UrlReferrer.AbsoluteUri.EndsWith(".largus.fr") || Request.UrlReferrer.AbsoluteUri.Contains("/auto/")))
                Session["ModifiedParcoursId"] = null;

            return View();
        }

        /// <summary>
        /// Récupérer les réponses du questionnaire et l'envoyer vers l'API de recherche pour retourner les bons résultats.
        /// </summary>
        /// <param name="parcours"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult ValidateParcours(ParcoursViewModel parcours)
        {
            var response = new Response<ModelViewModel>();
            try
            {
                if (parcours.BrandPreference != null)
                    parcours.BrandPreference = parcours.BrandPreference.ConvertAll(brand => brand != "all" ? brand.ToUpper() : "all");
                if (!string.IsNullOrEmpty(parcours.Parking))
                    if (parcours.Parking == "all")
                        parcours.Parking = "large";
                // Sérialisation de parcours.
                var parcoursJson = JsonConvert.SerializeObject(parcours);

                // Appeler le web service REST pour retourner les résultats du parcours.
                using (var httpClient = new HttpClient())
                {
                    //var stopwatch = new Stopwatch();
                    //stopwatch.Start();
                    httpClient.Timeout = new TimeSpan(0, 0, 20);
                    var parcoursApiUrl = ConfigurationManager.AppSettings["ParcoursApiUrl"];
                    var byteArray = Encoding.ASCII.GetBytes(ConfigurationManager.AppSettings["ParcoursAuthentication"]);
                    var header = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    httpClient.DefaultRequestHeaders.Authorization = header;
                    var content = new StringContent(parcoursJson, Encoding.UTF8, "application/json");
                    var httpResponse = httpClient.PostAsync(parcoursApiUrl, content).Result;
                    httpResponse.EnsureSuccessStatusCode();
                    var parcoursResultStr = httpResponse.Content.ReadAsStringAsync().Result;
                    //Désérialisation de résultat.
                    var parcoursResult = JsonConvert.DeserializeObject<ParcoursResultViewModel>(parcoursResultStr);
                    if (parcoursResult.Status.Any())
                    {
                        // S'il n'y a pas des annonces.
                        if (parcoursResult.Status[0] == 2)
                        {
                            response.IsSuccess = false;
                            response.Message = "Il n'y a pas des annonces de véhicules correspondant à votre besoin. Veuillez modifier vos sélections !";
                        }
                        else
                        {
                            var modelNb = int.Parse(ConfigurationManager.AppSettings["ModelNb"]);
                            var preferenceList = parcours.BrandPreference;
                            var autoList = GetModelListFromSearchApi(parcours, parcoursResult);
                            if (autoList.Any())
                            {
                                //var oidList = new List<string>();
                                if (parcours.BrandPreference.Any() && parcours.BrandPreference[0] != "all")
                                    autoList = autoList.OrderByDescending(x => preferenceList.Contains(x.Marque, StringComparer.OrdinalIgnoreCase)).ThenByDescending(x => x.Score).ThenByDescending(x => x.CountByCodeFamille).ToList();
                                else
                                    autoList = autoList.OrderByDescending(x => x.Score).ThenByDescending(x => x.CountByCodeFamille).ToList();

                                Session["AutoList"] = autoList;

                                var modelViewModel = new ModelViewModel
                                {
                                    OidList = string.Join(",", autoList.Where(x => x.CountByCodeFamille > 0).Select(x => x.CodeFamille).Distinct().Take(modelNb).ToList()),
                                    MarqueModelList = string.Join(",", autoList.Where(x => x.CountByCodeFamille > 0).Select(x => x.Marque + " " + x.Model + " nb : " + x.CountByCodeFamille).Distinct().Take(modelNb).ToList())
                                };

                                // Sauvegarder les données du parcours dans la base
                                using (var parcoursResponseContext = new ParcoursResponseContext())
                                {
                                    var parcoursResponse = new ParcoursResponse
                                    {
                                        CreationDate = DateTime.Now,
                                        Modif = Session["ModifiedParcoursId"] != null ? (int.TryParse(Session["ModifiedParcoursId"].ToString(), out int modifiedParcoursId) ? modifiedParcoursId : 0) : 0,
                                        CarBehaviour = parcours.CarBehaviour,
                                        PersonsNb = parcours.PersonsNb,
                                        TrunkSize = parcours.TrunkSize,
                                        Distance = parcours.Distance,
                                        DrivingPosition = parcours.DrivingPosition,
                                        Parking = parcours.Parking,
                                        BrandPreference = parcours.BrandPreference != null ? string.Join(",", parcours.BrandPreference).Replace("all", "Peu importe") : "Peu importe",
                                        Budget = GetBudget(parcours.Budget),
                                        YearMax = parcours.Year,
                                        ZipCode = parcours.Localisation != null ? parcours.Localisation.ZipCode : string.Empty,
                                        MaximumDistance = parcours.Localisation != null ? parcours.Localisation.MaximumDistance : 0
                                    };
                                    parcoursResponseContext.ParcoursResponse.Add(parcoursResponse);
                                    parcoursResponseContext.SaveChanges();
                                    Session["ModifiedParcoursId"] = null;
                                    Session["CurrentParcoursId"] = parcoursResponse.Id.ToString();
                                }
                                if (Session["CurrentParcoursId"] != null)
                                {
                                    var isCastValid = int.TryParse(Session["CurrentParcoursId"].ToString(), out int currentParcoursId);
                                    if (isCastValid)
                                    {
                                        // Sauvegarder les données du l'API de parcours dans la base
                                        using (var parcoursApiContext = new ParcoursApiContext())
                                        {
                                            var parcoursApiList = autoList.Select(x => new ParcoursApi
                                            {
                                                //Id = Guid.NewGuid(),
                                                IdParcours = currentParcoursId,

                                                Famille = $"{x.Marque.ToUpper()} {x.Model.ToUpper()}",
                                                Panier = x.Category,
                                                Score = x.Score,
                                                NbAnnoRappRef = x.CountByCodeFamille,
                                                NbAnnoTotal = x.Total,
                                                Prix = GetPrix(x),
                                                OidFamille = x.CodeFamille
                                            });
                                            parcoursApiContext.ParcoursApi.AddRange(parcoursApiList);
                                            parcoursApiContext.SaveChanges();
                                        }
                                    }
                                }

                                response.Data = modelViewModel;
                                response.IsSuccess = true;
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Message = "Il n'y a pas des annonces de véhicules correspondant à votre besoin. Veuillez modifier vos sélections !";
                            }
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = "Il n'y a pas des annonces de véhicules correspondant à votre besoin. Veuillez modifier vos sélections !";
                    }
                    //Logger.Info($"Le temps écoulé pour appler l'API de parcours est: {stopwatch.Elapsed}");

                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = $"Une exception est survenue. Message : {ex.Message}";
                Logger.Error($"Une exception est survenue. Message : {ex.Message}");
                return Json(response);
            }
        }

        /// <summary>
        /// Afficher la page "listing par modèle"
        /// </summary>
        /// <returns></returns>       
        public ActionResult Result()
        {
            try
            {
                var oids = string.Empty;
                var newAutoList = new List<Auto>();
                var parcours = new ParcoursViewModel();
                var filters = new FiltersViewModel();
                if (TempData["oids"] != null)
                {
                    oids = (string)TempData["oids"];
                    newAutoList = GetModelListByOids(oids);
                    var totalFound = newAutoList.Count();
                    ViewBag.TotalFound = totalFound;
                    ViewBag.ReloadModelPage = false;
                }
                else
                    // Si on fait un refresh de la page listing modèle. 
                    ViewBag.ReloadModelPage = true;

                if (TempData["ParcoursViewModel"] != null)
                    parcours = (ParcoursViewModel)TempData["ParcoursViewModel"];

                ViewBag.Filters = filters;
                if (parcours.BrandPreference.Any() && parcours.BrandPreference[0] != "all")
                    ViewBag.FoundMarque = newAutoList.Select(x => x.marque).Intersect(parcours.BrandPreference, StringComparer.OrdinalIgnoreCase).Any();
                else
                    ViewBag.FoundMarque = true;

                if (Session["CurrentParcoursId"] != null)
                {
                    Logger.Info($"l'id de parcours current est {Session["CurrentParcoursId"].ToString()}");
                    var isCastValid = int.TryParse(Session["CurrentParcoursId"].ToString(), out int currentParcoursId);
                    if (isCastValid)
                    {
                        using (var parcoursResponseContext = new ParcoursResponseContext())
                        {
                            var selectedParcours = parcoursResponseContext.ParcoursResponse.FirstOrDefault(x => x.Id == currentParcoursId);
                            var proposedModel=string.Join(",", newAutoList.Select(x => x.marque + " " + x.modele + " (nb : " + x.total + ")").ToList());
                            Logger.Info($"les modèles proposés pour parcours id {currentParcoursId} sont : {proposedModel}");
                            selectedParcours.ProposedModel = string.Join(",", newAutoList.Select(x => x.marque + " " + x.modele + " (nb : " + x.total + ")").ToList());
                            selectedParcours.NbModel = newAutoList.Count;
                            parcoursResponseContext.SaveChanges();
                        }
                    }

                }
                else
                    Logger.Info("l'id de parcours est null");
                return View(newAutoList);
            }
            catch (Exception ex)
            {
                Logger.Error($"Une exception est survenue. Message : {ex.Message}");
                throw ex;
            }
        }

        /// <summary>
        /// Afficher la page "listing par modèle" + enquête sur la satisfaction des utilisateurs. 
        /// </summary>
        /// <returns></returns>       
        public ActionResult ResultWithQuestion()
        {
            try
            {
                var oids = string.Empty;
                var newAutoList = new List<Auto>();
                var parcours = new ParcoursViewModel();
                var filters = new FiltersViewModel();
                if (TempData["oids"] != null)
                {
                    oids = (string)TempData["oids"];
                    newAutoList = GetModelListByOids(oids);
                    var totalFound = newAutoList.Count();
                    ViewBag.TotalFound = totalFound;
                    ViewBag.ReloadModelPage = false;
                }
                else
                    // Si on fait un refresh de la page listing modèle. 
                    ViewBag.ReloadModelPage = true;

                if (TempData["ParcoursViewModel"] != null)
                    parcours = (ParcoursViewModel)TempData["ParcoursViewModel"];

                ViewBag.Filters = filters;
                if (parcours.BrandPreference.Any() && parcours.BrandPreference[0] != "all")
                    ViewBag.FoundMarque = newAutoList.Select(x => x.marque).Intersect(parcours.BrandPreference, StringComparer.OrdinalIgnoreCase).Any();
                else
                    ViewBag.FoundMarque = true;

                return View(newAutoList);
            }
            catch (Exception ex)
            {
                Logger.Error($"Une exception est survenue. Message : {ex.Message}");
                throw ex;
            }
        }

        /// <summary>
        /// Afficher la page GridResult pour montrer tous les modèles retourner par L'API (pour un parcours donné)
        /// </summary>
        /// <param name="parcoursId"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult GridResult(int? parcoursId)
        {
            var autoList = new List<AutoViewModel>();
            try
            {
                if (parcoursId.HasValue)
                {
                    using (var parcoursApiContext = new ParcoursApiContext())
                    {
                        var parcoursApiList = parcoursApiContext.ParcoursApi.Where(x => x.IdParcours == parcoursId.Value).ToList();
                        ViewBag.ParcoursId = parcoursId.Value;
                        return View(parcoursApiList);
                    }
                }               
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Logger.Error($"Une exception est survenue. Message : {ex.Message}");
                throw ex;
            }
        }

        /// <summary>
        /// Afficher la page de la grille pour montrer tous les réponses de l'enquête sur la satisfaction des utilisateurs.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult QuizzResult()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                Logger.Error($"Une exception est survenue. Message : {ex.Message}");
                throw ex;
            }
        }

        /// <summary>
        /// Mettre les oids dans un TempData + Afficher la page "listing par modèle".
        /// </summary>
        /// <param name="oids"></param>
        /// <param name="parcours"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult GetResult(string oids, string parcours)
        {
            try
            {
                TempData["oids"] = oids;
                // Désérialisation de parcours.
                if (!string.IsNullOrEmpty(parcours))
                {
                    var parcoursViewModel = Utility.DeserializeParcours(parcours);
                    if (parcoursViewModel.BrandPreference != null)
                        parcoursViewModel.BrandPreference = parcoursViewModel.BrandPreference.ConvertAll(brand => brand != "all" ? brand.ToUpper() : "all");
                    if (!string.IsNullOrEmpty(parcoursViewModel.Parking))
                        if (parcoursViewModel.Parking == "all")
                            parcoursViewModel.Parking = "large";
                    TempData["ParcoursViewModel"] = parcoursViewModel;
                }
                return RedirectToAction("Result");
            }
            catch (Exception ex)
            {
                Logger.Error($"Une exception est survenue. Message : {ex.Message}");
                throw ex;
            }
        }

        /// <summary>
        /// Mettre les oids dans un TempData + Afficher la page "listing par modèle" + enquête sur la satisfaction des utilisateurs.
        /// </summary>
        /// <param name="oids"></param>
        /// <param name="parcours"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult GetResultWithQuestion(string oids, string parcours)
        {
            try
            {
                TempData["oids"] = oids;
                // Désérialisation de parcours.
                if (!string.IsNullOrEmpty(parcours))
                {
                    var parcoursViewModel = Utility.DeserializeParcours(parcours);
                    if (parcoursViewModel.BrandPreference != null)
                        parcoursViewModel.BrandPreference = parcoursViewModel.BrandPreference.ConvertAll(brand => brand != "all" ? brand.ToUpper() : "all");
                    if (!string.IsNullOrEmpty(parcoursViewModel.Parking))
                        if (parcoursViewModel.Parking == "all")
                            parcoursViewModel.Parking = "large";
                    TempData["ParcoursViewModel"] = parcoursViewModel;
                }
                return RedirectToAction("ResultWithQuestion");
            }
            catch (Exception ex)
            {
                Logger.Error($"Une exception est survenue. Message : {ex.Message}");
                throw ex;
            }
        }

        /// <summary>
        /// Sauvegarder les modèles séléctionnées et faire la mise à jour de parcours
        /// </summary>
        /// <param name="parcoursDetails"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult SaveParcoursDetails(ParcoursDetailsViewModel parcoursDetails)
        {
            var response = new Response<string>();
            try
            {
                if (Session["CurrentParcoursId"] != null)
                {
                    var isCastValid = int.TryParse(Session["CurrentParcoursId"].ToString(), out int currentParcoursId);
                    if (isCastValid)
                    {
                        using (var parcoursResponseContext = new ParcoursResponseContext())
                        {
                            var selectedParcours = parcoursResponseContext.ParcoursResponse.FirstOrDefault(x => x.Id == currentParcoursId);
                            selectedParcours.SelectedModel = parcoursDetails.SelectedModel;
                            parcoursResponseContext.SaveChanges();
                        }
                    }
                }
                response.IsSuccess = true;
                return Json(response);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = $"Une exception est survenue. Message : {ex.Message}";
                Logger.Error($"Une exception est survenue. Message : {ex.Message}");
                return Json(response);
            }
        }

        /// <summary>
        /// Retourner tous les détails de parcours fait par l’internaute
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult GetAllParcoursResponse(jQueryDataTableParamModel param)
        {
            try
            {
                using (var parcoursResponseContext = new ParcoursResponseContext())
                {
                    var parcoursResponseList = parcoursResponseContext.ParcoursResponse.ToList();
                    if (!string.IsNullOrEmpty(param.sSearch))
                        parcoursResponseList = parcoursResponseList.Where(
                            x => (x.ProposedModel.ToLower().Contains(param.sSearch.ToLower()))).OrderByDescending(x => x.CreationDate).ToList();

                    var nbTotalRecord = parcoursResponseList.Count;
                    //parcoursResponseList = parcoursResponseList.Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                    var isCreationDateSortable = Convert.ToBoolean(Request["bSortable_0"]);
                    var isIdSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var isModifSortable = Convert.ToBoolean(Request["bSortable_2"]);

                    var isCarBehaviourSortable = Convert.ToBoolean(Request["bSortable_3"]);
                    var isTrunkSizeSortable = Convert.ToBoolean(Request["bSortable_5"]);
                    var isDistanceSortable = Convert.ToBoolean(Request["bSortable_6"]);
                    var isDrivingPositionSortable = Convert.ToBoolean(Request["bSortable_7"]);
                    var isParkingSortable = Convert.ToBoolean(Request["bSortable_8"]);
                    var isYearMaxSortable = Convert.ToBoolean(Request["bSortable_11"]);

                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    var sortDirection = Request["sSortDir_0"]; // asc or desc

                    switch (sortColumnIndex)
                    {
                        case 0:
                            Func<ParcoursResponse, DateTime> orderingByDateFunction = (c => sortColumnIndex == 0 && isCreationDateSortable ? c.CreationDate : DateTime.Now);
                            parcoursResponseList = sortDirection == "asc" ?
                                parcoursResponseList.OrderBy(orderingByDateFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList() :
                                parcoursResponseList.OrderByDescending(orderingByDateFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                            break;
                        case 1:
                        case 2:
                            Func<ParcoursResponse, int> orderingByIntFunction = (c => sortColumnIndex == 1 && isIdSortable ? c.Id :
                                                                  sortColumnIndex == 2 && isModifSortable ? c.Modif :
                                                                  0);
                            parcoursResponseList = sortDirection == "asc" ?
                                parcoursResponseList.OrderBy(orderingByIntFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList() :
                                parcoursResponseList.OrderByDescending(orderingByIntFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                            break;
                        default:
                            Func<ParcoursResponse, string> orderingByStrFunction = (c => sortColumnIndex == 3 && isCarBehaviourSortable ? c.CarBehaviour :
                                                                  sortColumnIndex == 5 && isTrunkSizeSortable ? c.TrunkSize :
                                                                  sortColumnIndex == 6 && isDistanceSortable ? c.Distance :
                                                                  sortColumnIndex == 7 && isDrivingPositionSortable ? c.DrivingPosition :
                                                                  sortColumnIndex == 8 && isParkingSortable ? c.Parking :
                                                                  sortColumnIndex == 11 && isYearMaxSortable ? c.YearMax :
                                                                  "");
                            parcoursResponseList = sortDirection == "asc" ?
                                parcoursResponseList.OrderBy(orderingByStrFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList() :
                                parcoursResponseList.OrderByDescending(orderingByStrFunction).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                            break;
                    }
                    //parcoursResponseList.ForEach(c => c.Budget = c.Budget == "0,0" ? "Peu importe" : c.Budget);
                    var result = parcoursResponseList.Select(x => new ParcoursResponseViewModel
                    {
                        CreationDate = x.CreationDate.ToString("f", CultureInfo.CreateSpecificCulture("fr-FR")),
                        Id = x.Id,
                        Modif = x.Modif,

                        CarBehaviour = x.CarBehaviour,
                        PersonsNb = x.PersonsNb,
                        TrunkSize = x.TrunkSize,
                        Distance = x.Distance,
                        DrivingPosition = x.DrivingPosition,
                        Parking = x.Parking,
                        BrandPreference = x.BrandPreference,
                        Budget = x.Budget == "0,0" ? "Peu importe" : x.Budget,
                        YearMax = x.YearMax,
                        Localisation = GetLocalisation(x.ZipCode, x.MaximumDistance),
                        ProposedModel = x.ProposedModel,
                        SelectedModel = x.SelectedModel,
                        NbModel = x.NbModel
                    }).ToList();
                    return Json(new
                    {
                        data = result,
                        iTotalRecords = nbTotalRecord,
                        iTotalDisplayRecords = nbTotalRecord,
                        success = true
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("An error occurred while loading list of contacts by meeting. Message: {0}", ex.Message));
                return Json(new
                {
                    success = false
                });
            }
        }
        /// <summary>
        /// Télécharger le rapport CSV de tous les parcours
        /// </summary>
        /// <returns></returns>
        public FileContentResult DownloadDetailsParcoursCSV()
        {
            var csvStr = new StringBuilder();
            using (var parcoursResponseContext = new ParcoursResponseContext())
            {
                var parcoursResponseList = parcoursResponseContext.ParcoursResponse.ToList();
                foreach (var parcours in parcoursResponseList)
                    csvStr.AppendFormat($"{parcours.CreationDate.ToString("f", CultureInfo.CreateSpecificCulture("fr-FR"))};{parcours.Id};{parcours.Modif};{parcours.CarBehaviour};{parcours.PersonsNb};{parcours.TrunkSize};{parcours.Distance};{parcours.DrivingPosition};{parcours.Parking};{parcours.BrandPreference};{parcours.Budget};{parcours.YearMax};{GetLocalisation(parcours.ZipCode, parcours.MaximumDistance)};{parcours.ProposedModel};{parcours.SelectedModel};{parcours.NbModel};{Environment.NewLine}");

            }
            return File(new UTF8Encoding().GetBytes(csvStr.ToString()), "text/csv", "Report.csv");
        }

        /// <summary>
        /// Télécharger le rapport CSV d'un parcours
        /// </summary>
        /// <returns></returns>
        public FileContentResult DownloadParcoursCSV(int parcoursId)
        {
            var csvStr = new StringBuilder();
            using (var parcoursApiContext = new ParcoursApiContext())
            {
                var parcoursApiList = parcoursApiContext.ParcoursApi.Where(x => x.IdParcours == parcoursId).ToList();
                var percentStr = string.Empty;
                double percent = 0;
                foreach (var parcoursApi in parcoursApiList)
                {
                    if (parcoursApi.NbAnnoTotal != 0 && parcoursApi.NbAnnoRappRef != 0 && parcoursApi.NbAnnoTotal >= parcoursApi.NbAnnoRappRef)
                    {
                        if (parcoursApi.NbAnnoTotal == parcoursApi.NbAnnoRappRef)
                            percentStr = "100 %";
                        else
                        {
                            percent = ((double)parcoursApi.NbAnnoRappRef / parcoursApi.NbAnnoTotal) * 100;
                            percentStr = percent.ToString("F") + " %";
                        }
                    }
                    else
                        percentStr = string.Empty;

                    csvStr.AppendFormat($"{parcoursApi.Famille};{parcoursApi.Panier};{parcoursApi.Score};{parcoursApi.NbAnnoRappRef};{parcoursApi.NbAnnoTotal};{percentStr};{parcoursApi.Prix};{parcoursApi.OidFamille};{Environment.NewLine}");
                }
            }
            return File(new UTF8Encoding().GetBytes(csvStr.ToString()), "text/csv", "Report.csv");
        }
        /// <summary>
        /// Faire la mise à jour de la session pour id de parcours initial
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public void UpdateParcoursSession()
        {
            if (Session["CurrentParcoursId"] != null)
            {
                var isCastValid = int.TryParse(Session["CurrentParcoursId"].ToString(), out int currentParcoursId);
                if (isCastValid)
                {
                    using (var parcoursResponseContext = new ParcoursResponseContext())
                    {
                        var selectedParcours = parcoursResponseContext.ParcoursResponse.FirstOrDefault(x => x.Id == currentParcoursId);
                        Session["ModifiedParcoursId"] = selectedParcours.Modif > 0 ? selectedParcours.Modif.ToString() : Session["CurrentParcoursId"].ToString();
                    }
                }
            }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Remplir le filter à travers le parcours.
        /// </summary>
        /// <param name="parcours"></param>
        /// <returns></returns>
        private FiltersViewModel GetFiltersFromParcours(ParcoursViewModel parcours)
        {
            var filters = new FiltersViewModel();
            if (parcours.Budget.Any())
            {
                if (!(parcours.Budget[0] == 0 && parcours.Budget[1] == 0))
                {
                    filters.Items["prix"].Min = parcours.Budget[0];
                    filters.Items["prix"].Max = parcours.Budget[1];
                }
            }

            if (!string.IsNullOrEmpty(parcours.Year))
            {
                var currentYear = DateTime.Now.Year;
                switch (parcours.Year)
                {
                    case "recent":
                        filters.Items["annee"].Max = currentYear;
                        filters.Items["annee"].Min = currentYear - 3;
                        break;
                    case "old":
                        filters.Items["annee"].Max = currentYear - 4;
                        filters.Items["annee"].Min = currentYear - 12;
                        filters.Items["state"].Value = "1";
                        break;
                    default:
                        break;
                }
            }

            if (parcours.BrandPreference.Any() && parcours.BrandPreference[0] != "all")
                filters.Items["marque"].Value = string.Join(",", parcours.BrandPreference);
            if (!string.IsNullOrEmpty(parcours.Localisation?.ZipCode))
            {
                filters.Items["location"].Location.ZipCode = parcours.Localisation.ZipCode;
                if (parcours.Localisation.MaximumDistance > 0)
                    filters.Items["location"].Location.Distance = parcours.Localisation.MaximumDistance;
            }
            return filters;
        }
        /// <summary>
        /// Récupérer tous les modèles retournés par l'API REST de parcours
        /// </summary>
        /// <param name="parcours"></param>
        /// <param name="parcoursResult"></param>
        /// <returns></returns>
        private List<AutoViewModel> GetModelListFromSearchApi(ParcoursViewModel parcours, ParcoursResultViewModel parcoursResult)
        {
            var result = new List<AutoViewModel>();
            var marqueList = parcoursResult.Family.LabelList.Select(x => x.Split('/')[0]).ToList();
            var modelList = parcoursResult.Family.LabelList.Select(x => x.Split('/')[1]).ToList();
            for (int i = 0; i < parcoursResult.Family.OidList.Count; i++)
            {
                var autoViewModel = new AutoViewModel()
                {
                    Marque = marqueList[i],
                    Model = modelList[i],
                    Category = parcoursResult.Family.CategoryList[i].ToString(),
                    CodeFamille = parcoursResult.Family.OidList[i],
                    Score = parcoursResult.Family.ScoreList[i]
                };
                var filters = GetFiltersFromParcours(parcours);
                filters.Items["modele"].Value = autoViewModel.Model;
                filters.Items["marque"].Value = autoViewModel.Marque;
                var autoList = SearchRepository.SimpleSearch<Auto>(filters, "modele, min(prix) as minimum, max(prix) as maximum, rsi_code_phase_evo, annee_modele, count(*) as total", "modele", "total desc");
                //if (autoList.List.Any(x => string.Equals(x.modele, autoViewModel.Model, StringComparison.OrdinalIgnoreCase)))
                if (autoList.List.Any())
                {
                    //var selectedAuto = autoList.List.FirstOrDefault(x => string.Equals(x.modele, autoViewModel.Model, StringComparison.OrdinalIgnoreCase));
                    var selectedAuto = autoList.List.FirstOrDefault();
                    autoViewModel.IsFound = true;
                    autoViewModel.Total = selectedAuto.total;
                    autoViewModel.CodePhase = selectedAuto.rsi_code_phase_evo;
                    autoViewModel.Annee = selectedAuto.annee_modele;
                    filters.Items["oid_famille"].Value = autoViewModel.CodeFamille.ToString();
                    var queryList = SearchRepository.SimpleSearch<Auto>(filters, "rsi_code_famille, min(prix) as minimum, max(prix) as maximum, count(*) as total", "rsi_code_famille", "total desc");
                    //var queryList = SearchRepository.ExecuteQuery<Auto>("select count(*) as interprete1 from auto where match ('@rsi_code_famille \"^" + autoViewModel.CodeFamille + "$\"  ') limit 0,1");
                    if (queryList.List.Any())
                    {
                        autoViewModel.CountByCodeFamille = queryList.List.FirstOrDefault().total;
                        autoViewModel.Minimum = queryList.List.FirstOrDefault().minimum;
                        autoViewModel.Maximum = queryList.List.FirstOrDefault().maximum;
                    }
                }
                result.Add(autoViewModel);
            }
            return result;
        }
        private List<Auto> GetModelListByOids(string oids)
        {
            var newAutoList = new List<Auto>();
            var parcours = new ParcoursViewModel();
            var filters = new FiltersViewModel();
            if (!string.IsNullOrEmpty(oids))
            {
                if (TempData["ParcoursViewModel"] != null)
                    parcours = (ParcoursViewModel)TempData["ParcoursViewModel"];
                // Remplir les filtres à partir des reponses de l'utilisateur
                if (TempData["ParcoursViewModel"] != null)
                    filters = GetFiltersFromParcours(parcours);
                filters.Items["oid_famille"].Value = oids;
                filters.Items["marque"].Value = null;

                var modelNb = int.Parse(ConfigurationManager.AppSettings["ModelNb"]);
                var autoList = SearchRepository.SimpleSearch<Auto>(filters, "rsi_code_famille,  marque, modele, finition, min(prix) as minimum, max(prix) as maximum, count(*) as total", "rsi_code_famille", "total desc");
                var yearList = new List<int>();
                var currentYear = DateTime.Now.Year;
                if (parcours.Year == "old")
                {
                    for (int i = 4; i < 13; i++)
                    {
                        yearList.Add(currentYear - i);
                    }
                    yearList.Reverse();
                }
                // Récupérer tous les phase d'une famille pour afficher la photo du modèle
                foreach (var auto in autoList.List)
                {
                    var result = SearchRepository.ExecuteQuery<Auto>(" select rsi_code_phase_evo, annee_modele  from auto where match ('@rsi_code_famille \" ^ " + auto.rsi_code_famille + "$\" ') group by rsi_code_phase_evo order by annee_modele desc ");

                    if (parcours.Year == "old")
                        result = result.OrderByDescending(x => yearList.IndexOf(x.annee_modele)).ThenByDescending(x => x.annee_modele < currentYear - 12).ToList();

                    auto.rsi_code_phase_evo = string.Join(",", result.Select(x => x.rsi_code_phase_evo).ToList());
                    auto.AnneeList = string.Join(",", result.Select(x => x.annee_modele).ToList());
                    auto.KeyList = string.Join(",", result.Select(x => Misc.ArgusPhotoUrlKey(x.rsi_code_phase_evo, x.annee_modele)).ToList());
                }

                newAutoList = autoList.List.OrderBy(x => oids.IndexOf(x.rsi_code_famille)).ToList();
            }
            return newAutoList;
        }
        /// <summary>
        /// Formater le budget => "code postal = cp, dans un rayon de r km" ou "Peu importe"
        /// </summary>
        /// <param name="zipCode"></param>
        /// <param name="maxDistance"></param>
        /// <returns></returns>
        private string GetLocalisation(string zipCode, int? maxDistance)
        {
            var localisation = string.Empty;
            if (!string.IsNullOrEmpty(zipCode))
            {
                localisation += "code postal = " + zipCode;
                if (maxDistance.HasValue)
                    localisation += ", dans un rayon de " + maxDistance.Value + " KM";
            }
            else
                localisation = "Peu importe";

            return localisation;
        }
        /// <summary>
        /// Formater le budget => "(min,max)" ou "Peu importe"
        /// </summary>
        /// <param name="budgetList"></param>
        /// <returns></returns>
        private string GetBudget(List<int> budgetList)
        {
            var budget = string.Empty;
            if (budget.Any())
            {
                if (budgetList.Count == 2)
                    budget = (budgetList[0] == budgetList[1]) && (budgetList[0] == 0) ? "Peu importe" : string.Join(";", budgetList);
            }
            else
                budget = "Peu importe";
            return budget;
        }

        /// <summary>
        /// Formater le prix 
        /// </summary>
        /// <param name="auto"></param>
        /// <returns></returns>
        private string GetPrix(AutoViewModel auto)
        {
            switch (auto.CountByCodeFamille)
            {
                case 0:
                    return string.Empty;
                case 1:
                    return $"{auto.Minimum.ToString("# ###")} €";
                default:
                    return $"{auto.Minimum.ToString("# ###")} € à {auto.Maximum.ToString("# ###")} €";
            }
        }
        #endregion
    }
}