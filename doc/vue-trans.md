# Traduction avec Symfony et Vue 2 + VueI18n

#### Avant tout
Dans votre template twig, il est nécessaire de faire appel à `{{ parent() }}` dans votre 
`{%block javascripts%}`, sinon la locale ne sera pas disponible côté Vue (cf. template base).

#### Description

Cette installation de Vue 2 utilise VueI18n pour les traductions. Ce plugin bien
pratique propose 2 fonctions de template bien pratique, `$t()` et `$tc()` qui
correspondent aux méthodes `trans` et `transchoice` de Symfony.

Une commande, `bin/console vue:trans:dump` permet de générer les traductions VueI18n à
partir des fichiers `xliff` de Symfony.
/!\ Attention, les traductions générées seront identifiées uniquement par l'id, donc :
* il faut utiliser un **id** différent pour chaque traduction
* il est conseillé de préfixer vos **id** de traduction selon leur domaine

Malheureusement, VueI18n ne prend pas en charge
les *intervalles de pluralisation explicites* - voir [Explicit Interval Pluralization](https://symfony.com/doc/current/components/translation/usage.html#explicit-interval-pluralization).
Bien que la commande génère des traductions utilisables par VueI18n, il est recommandé de n'utiliser
que les formats gérés par VueI18n, afin d'assurer un comportement isomorphe PHP / JS.

#### Bad practice
Ceci sera utilisable et correct en PHP :
```xml
<trans-unit id="apple">
    <source>
        {0}no apples |{1}one apple | {2,3,4} %count% apples |
        ]5,+Inf] many apples
    </source>
    <target>
        {0}aucune pomme |{1}une pomme | {2,3,4} %count% pomme |
        ]5,+Inf] beaucoup de pommes
    </target>
</trans-unit>
```
ce qui donnera le fichier translations.json suivant :
```json
{
    "fr": {
        "apple": "aucune pomme | une pomme | %count% pommes | beaucoup de pommes"
    }
}
```
Malheureusement, le dernier cas ne pourra jamais être pris en compte : en effet, le *transchoice* de VueI18n,
`$tc`, ne supporte que 2 cas de figure :
* 2 choix : singulier pour le premier, pluriel pour le second
* 3 choix : nul pour le premier, singulier pour le deuxième, pluriel pour le troisième

#### Good practice
```xml
<trans-unit id="apple">
    <source>{0}no apples |{1}one apple |]1, +Inf] %count% apples</source>
    <target>{0}aucune pomme |{1}une pomme |]1, +Inf] %count% pommes</target>
</trans-unit>
```
ce qui donnera le fichier translations.json suivant :
```json
{
    "fr": {
        "apple": "aucune pomme | une pomme | %count% pommes"
    }
}
```
Utilisable ensuite ainsi (template)
```javascript
// 
$tc('apple', 0)
// affiche: aucune pomme
$tc('apple', 1)
// affiche: une pomme
$tc('apple', x) // avec x > 1
// affiche: x pommes
```

Si vous avez plus de 3 types de pluralisation, il vous faudra donc utiliser plusieurs traductions différentes


#### Filtre trans
Sur l'application en l'état, un filter Vue a été ajouté afin de simplifier la traduction et de garder un
comportement plus proche de celui de Symfony.
Le filtre `trans` remplace à la fois le `trans` et le `transchoice` de Symfony :

```vuejs
message|trans(vars)         // retourne message ou message[1] si choix
message|trans(0)            // retourne le premier choix
```

Si on passe `{count/* ... */}` en premier argument, `choice` sera automatiquement initialisé avec cette
valeur. Inversement, il est inutile de passer de variables lorsque la seule variable utilisée est `count`
avec choice pour valeur.

----

[Retour au sommaire](../README.md)