# SSR avec Symfony + Vue2

#### Paquets requis
Les paquets npm sont déjà installés mais certaines déepnedances doivent être disponibles côté
serveur :
* vue-server-renderer

#### Routes
Si tout le site était basé sur une application Vue, le routing serait uniquement géré par Vue
et Symfony jouerait uniquement les rôles de "front controller" et d'API.
Ici seules 2 types de pages (et donc composants) sont concernés : *parcours par besoin*
et *recherche* (listing).
Il faut donc dupliquer ces routes côté Vue et côté Symfony. Le routing Vue se paramètre dans 
`assets/js/lib/router.js`

#### Service JsRenderer
Ce service exécute le JS de l'application Vue et sert le html compilé. Il peut (et doit)
être appelé directement depuis Twig sous la forme suivante :
```twig
{{ js_renderer|raw }}
```
dans l'élément qui héberge votre application Vue.

That's all folks!

##### Passer des data à Vue
Pour hydrater l'application Vue, on stocke les données dans Vuex, le store. Dans le controller
Symfony, il suffit d'appeler
```php
JsRenderer::setInitialState($associative_array);
```
pour remplir celui-ci. Le contenu des variables du store correspondant aux clés du tableau en 
argument sera alors écrasé par vos données.

Pour que le store soit également hydraté "côté front" il est nécessaire de faire appel
à `{{ parent() }}` dans votre `{%block javascripts %}` (cf. template base).